-- Creates database 'digitales_fahrtenbuch_db' with all tables

CREATE DATABASE digitales_fahrtenbuch_db;
USE digitales_fahrtenbuch_db;

-- CONSTANTS
CREATE TABLE `digitales_fahrtenbuch_db`.`car_type` (
  `type_id` INT UNSIGNED NOT NULL,
  `name` NVARCHAR(30) NOT NULL,
  UNIQUE INDEX `type_id_UNIQUE` (`type_id` ASC) VISIBLE,
  PRIMARY KEY (`type_id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE);

CREATE TABLE `digitales_fahrtenbuch_db`.`role` (
  `role_id` INT UNSIGNED NOT NULL,
  `name` NVARCHAR(16) NOT NULL,
  PRIMARY KEY (`role_id`),
  UNIQUE INDEX `role_id_UNIQUE` (`role_id` ASC) VISIBLE,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE);

CREATE TABLE `digitales_fahrtenbuch_db`.`booking_state` (
  `state_id` INT UNSIGNED NOT NULL,
  `name` NVARCHAR(16) NOT NULL,
  PRIMARY KEY (`state_id`),
  UNIQUE INDEX `state_id_UNIQUE` (`state_id` ASC) VISIBLE,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE);

-- Tables

CREATE TABLE `digitales_fahrtenbuch_db`.`location` (
  `location_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `street` NVARCHAR(100) NOT NULL,
  `house_number` VARCHAR(5) NULL,
  `town` NVARCHAR(60) NOT NULL,
  `postalcode` VARCHAR(5) NOT NULL,
  PRIMARY KEY (`location_id`),
  UNIQUE INDEX `location_id_UNIQUE` (`location_id` ASC) VISIBLE);

CREATE TABLE `digitales_fahrtenbuch_db`.`passenger` (
  `passenger_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` NVARCHAR(50) NOT NULL,
  `last_name` NVARCHAR(50) NOT NULL,
  PRIMARY KEY (`passenger_id`),
  UNIQUE INDEX `passenger_id_UNIQUE` (`passenger_id` ASC) VISIBLE);

CREATE TABLE `digitales_fahrtenbuch_db`.`trip_part` (
  `part_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `start_datetime` DATETIME NOT NULL,
  `end_datetime` DATETIME NOT NULL,
  `distance` DECIMAL(5,1) UNSIGNED NOT NULL,
  `start_location_id` INT UNSIGNED NOT NULL,
  `end_location_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`part_id`),
  UNIQUE INDEX `part_id_UNIQUE` (`part_id` ASC) VISIBLE,
  INDEX `start_location_id_idx` (`start_location_id` ASC) VISIBLE,
  INDEX `end_location_id_idx` (`end_location_id` ASC) VISIBLE,
  CONSTRAINT `start_location_key_trip_part`
    FOREIGN KEY (`start_location_id`)
    REFERENCES `digitales_fahrtenbuch_db`.`location` (`location_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `end_location_id_key_trip_part`
    FOREIGN KEY (`end_location_id`)
    REFERENCES `digitales_fahrtenbuch_db`.`location` (`location_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `digitales_fahrtenbuch_db`.`car` (
  `car_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `model` NVARCHAR(50) NOT NULL,
  `manufacturer` NVARCHAR(50) NOT NULL,
  `vin` VARCHAR(17) NOT NULL,
  `seats` INT UNSIGNED NOT NULL,
  `milage` INT UNSIGNED NOT NULL,
  `first_registration` DATE NOT NULL,
  `type_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`car_id`),
  UNIQUE INDEX `car_id_UNIQUE` (`car_id` ASC) VISIBLE,
  INDEX `type_id_key_car_idx` (`type_id` ASC) VISIBLE,
  CONSTRAINT `type_id_key_car`
    FOREIGN KEY (`type_id`)
    REFERENCES `digitales_fahrtenbuch_db`.`car_type` (`type_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `digitales_fahrtenbuch_db`.`institution` (
  `institution_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` NVARCHAR(100) NOT NULL,
  `location` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`institution_id`),
  UNIQUE INDEX `institution_id_UNIQUE` (`institution_id` ASC) VISIBLE,
  UNIQUE INDEX `title_UNIQUE` (`title` ASC) VISIBLE,
  INDEX `location_key_institution_idx` (`location` ASC) VISIBLE,
  CONSTRAINT `location_key_institution`
    FOREIGN KEY (`location`)
    REFERENCES `digitales_fahrtenbuch_db`.`location` (`location_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `digitales_fahrtenbuch_db`.`user` (
  `user_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` NVARCHAR(50) NOT NULL,
  `last_name` NVARCHAR(50) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `is_blocked` TINYINT(1) UNSIGNED NULL DEFAULT '1',
  `institution` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE INDEX `user_id_UNIQUE` (`user_id` ASC) VISIBLE,
  INDEX `institution_key_user_idx` (`institution` ASC) VISIBLE,
  CONSTRAINT `institution_key_user`
    FOREIGN KEY (`institution`)
    REFERENCES `digitales_fahrtenbuch_db`.`institution` (`institution_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `digitales_fahrtenbuch_db`.`has_role` (
  `user_id` INT UNSIGNED NOT NULL,
  `role_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`user_id`, `role_id`),
  INDEX `role_key_has_role_idx` (`role_id` ASC) VISIBLE,
  CONSTRAINT `user_key_has_role`
    FOREIGN KEY (`user_id`)
    REFERENCES `digitales_fahrtenbuch_db`.`user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `role_key_has_role`
    FOREIGN KEY (`role_id`)
    REFERENCES `digitales_fahrtenbuch_db`.`role` (`role_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `digitales_fahrtenbuch_db`.`booking` (
  `booking_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `start_datetime` DATETIME NOT NULL,
  `end_datetime` DATETIME NOT NULL,
  `car_id` INT UNSIGNED NOT NULL,
  `user_id` INT UNSIGNED NOT NULL,
  `state_id` INT UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`booking_id`),
  UNIQUE INDEX `booking_id_UNIQUE` (`booking_id` ASC) VISIBLE,
  INDEX `car_id_key_booking_idx` (`car_id` ASC) VISIBLE,
  INDEX `user_id_key_booking_idx` (`user_id` ASC) VISIBLE,
  INDEX `state_id_key_booking_idx` (`state_id` ASC) VISIBLE,
  CONSTRAINT `car_id_key_booking`
    FOREIGN KEY (`car_id`)
    REFERENCES `digitales_fahrtenbuch_db`.`car` (`car_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `user_id_key_booking`
    FOREIGN KEY (`user_id`)
    REFERENCES `digitales_fahrtenbuch_db`.`user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `state_id_key_booking`
    FOREIGN KEY (`state_id`)
    REFERENCES `digitales_fahrtenbuch_db`.`booking_state` (`state_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `digitales_fahrtenbuch_db`.`trip_away` (
  `part_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`part_id`),
  UNIQUE INDEX `part_id_UNIQUE` (`part_id` ASC) VISIBLE,
  CONSTRAINT `part_id_key_trip_away`
    FOREIGN KEY (`part_id`)
    REFERENCES `digitales_fahrtenbuch_db`.`trip_part` (`part_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `digitales_fahrtenbuch_db`.`trip_back` (
  `part_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`part_id`),
  UNIQUE INDEX `part_id_UNIQUE` (`part_id` ASC) VISIBLE,
  CONSTRAINT `part_id_key_trip_back`
    FOREIGN KEY (`part_id`)
    REFERENCES `digitales_fahrtenbuch_db`.`trip_part` (`part_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `digitales_fahrtenbuch_db`.`trip` (
  `trip_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` NVARCHAR(100) NULL,
  `trip_away_id` INT UNSIGNED,
  `trip_back_id` INT UNSIGNED,
  `user_id` INT UNSIGNED NOT NULL,
  `car_id` INT UNSIGNED NOT NULL,
  `booking_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`trip_id`),
  UNIQUE INDEX `trip_id_UNIQUE` (`trip_id` ASC) VISIBLE,
  INDEX `trip_away_id_key_trip_idx` (`trip_away_id` ASC) VISIBLE,
  INDEX `trip_back_id_key_trip_idx` (`trip_back_id` ASC) VISIBLE,
  INDEX `user_id_key_trip_idx` (`user_id` ASC) VISIBLE,
  INDEX `car_id_key_trip_idx` (`car_id` ASC) VISIBLE,
  INDEX `booking_id_key_trip_idx` (`booking_id` ASC) VISIBLE,
  CONSTRAINT `booking_id_key_trip`
    FOREIGN KEY (`booking_id`)
    REFERENCES `digitales_fahrtenbuch_db`.`booking` (`booking_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `trip_away_id_key_trip`
    FOREIGN KEY (`trip_away_id`)
    REFERENCES `digitales_fahrtenbuch_db`.`trip_away` (`part_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `trip_back_id_key_trip`
    FOREIGN KEY (`trip_back_id`)
    REFERENCES `digitales_fahrtenbuch_db`.`trip_back` (`part_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `user_id_key_trip`
    FOREIGN KEY (`user_id`)
    REFERENCES `digitales_fahrtenbuch_db`.`user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `car_id_key_trip`
    FOREIGN KEY (`car_id`)
    REFERENCES `digitales_fahrtenbuch_db`.`car` (`car_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `digitales_fahrtenbuch_db`.`has_passenger` (
  `trip_id` INT UNSIGNED NOT NULL,
  `passenger_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`trip_id`, `passenger_id`),
  INDEX `passenger_id_idx` (`passenger_id` ASC) VISIBLE,
  CONSTRAINT `trip_id_key_has_passenger`
    FOREIGN KEY (`trip_id`)
    REFERENCES `digitales_fahrtenbuch_db`.`trip` (`trip_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `passenger_id_key_has_passenger`
    FOREIGN KEY (`passenger_id`)
    REFERENCES `digitales_fahrtenbuch_db`.`trip_part` (`part_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `digitales_fahrtenbuch_db`.`gas` (
  `gas_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `liter` DECIMAL(4,1) UNSIGNED NOT NULL,
  `amount` DECIMAL(5,2) UNSIGNED NOT NULL,
  `trip_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`gas_id`),
  UNIQUE INDEX `gas_id_UNIQUE` (`gas_id` ASC) VISIBLE,
  INDEX `trip_id_idx` (`trip_id` ASC) VISIBLE,
  CONSTRAINT `trip_id_key_gas`
    FOREIGN KEY (`trip_id`)
    REFERENCES `digitales_fahrtenbuch_db`.`trip` (`trip_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `digitales_fahrtenbuch_db`.`accident` (
  `accident_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `description` NVARCHAR(500) NOT NULL,
  `trip_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`accident_id`),
  UNIQUE INDEX `accident_id_UNIQUE` (`accident_id` ASC) VISIBLE,
  INDEX `trip_id_key_accident_idx` (`trip_id` ASC) VISIBLE,
  CONSTRAINT `trip_id_key_accident`
    FOREIGN KEY (`trip_id`)
    REFERENCES `digitales_fahrtenbuch_db`.`trip` (`trip_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `digitales_fahrtenbuch_db`.`trip_between` (
  `part_id` INT UNSIGNED NOT NULL,
  `trip_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`part_id`),
  UNIQUE INDEX `part_id_UNIQUE` (`part_id` ASC) VISIBLE,
  INDEX `trip_id_key_trip_between_idx` (`trip_id` ASC) VISIBLE,
  CONSTRAINT `part_id_key_trip_between`
    FOREIGN KEY (`part_id`)
    REFERENCES `digitales_fahrtenbuch_db`.`trip_part` (`part_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `trip_id_key_trip_between`
    FOREIGN KEY (`trip_id`)
    REFERENCES `digitales_fahrtenbuch_db`.`trip` (`trip_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `digitales_fahrtenbuch_db`.`sessions` (
  `session_id` VARCHAR(255) NOT NULL,
      `email` VARCHAR(255) NOT NULL,
      `expriry` DATETIME NOT NULL,
      PRIMARY KEY (`session_id`),
      UNIQUE INDEX `session_id_UNIQUE` (`session_id` ASC) VISIBLE,
      UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE);

-- Trigger

DROP TRIGGER IF EXISTS `digitales_fahrtenbuch_db`.`trip_away_BEFORE_INSERT`;
DELIMITER $$
USE `digitales_fahrtenbuch_db`$$
CREATE DEFINER = CURRENT_USER TRIGGER `trip_away_BEFORE_INSERT` BEFORE INSERT ON `trip_away` FOR EACH ROW BEGIN
	DECLARE var_part_id_in_trip_back INT;
    DECLARE var_part_id_in_trip_between INT;
    SELECT COUNT(*) part_id INTO var_part_id_in_trip_back FROM digitales_fahrtenbuch_db.trip_back WHERE part_id = NEW.part_id;
    SELECT COUNT(*) part_id INTO var_part_id_in_trip_between FROM digitales_fahrtenbuch_db.trip_between WHERE part_id = NEW.part_id;
    IF var_part_id_in_trip_back != 0
    THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Die part_id wurde bereits einem trip_back zugeordnet!';
	ELSEIF var_part_id_in_trip_between != 0
    THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Die part_id wurde bereits einem trip_between zugeordnet!';
	END IF;
END$$
DELIMITER ;

DROP TRIGGER IF EXISTS `digitales_fahrtenbuch_db`.`trip_back_BEFORE_INSERT`;
DELIMITER $$
USE `digitales_fahrtenbuch_db`$$
CREATE DEFINER = CURRENT_USER TRIGGER `digitales_fahrtenbuch_db`.`trip_back_BEFORE_INSERT` BEFORE INSERT ON `trip_back` FOR EACH ROW
BEGIN
	DECLARE var_part_id_in_trip_away INT;
    DECLARE var_part_id_in_trip_between INT;
    SELECT COUNT(*) part_id INTO var_part_id_in_trip_away FROM digitales_fahrtenbuch_db.trip_away WHERE part_id = NEW.part_id;
    SELECT COUNT(*) part_id INTO var_part_id_in_trip_between FROM digitales_fahrtenbuch_db.trip_between WHERE part_id = NEW.part_id;
    IF var_part_id_in_trip_away != 0
    THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Die part_id wurde bereits einem trip_away zugeordnet!';
	ELSEIF var_part_id_in_trip_between != 0
    THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Die part_id wurde bereits einem trip_between zugeordnet!';
	END IF;
END$$
DELIMITER ;

DROP TRIGGER IF EXISTS `digitales_fahrtenbuch_db`.`trip_between_BEFORE_INSERT`;
DELIMITER $$
USE `digitales_fahrtenbuch_db`$$
CREATE DEFINER = CURRENT_USER TRIGGER `digitales_fahrtenbuch_db`.`trip_between_BEFORE_INSERT` BEFORE INSERT ON `trip_between` FOR EACH ROW
BEGIN
	DECLARE var_part_id_in_trip_away INT;
    DECLARE var_part_id_in_trip_back INT;
    SELECT COUNT(*) part_id INTO var_part_id_in_trip_away FROM digitales_fahrtenbuch_db.trip_away WHERE part_id = NEW.part_id;
    SELECT COUNT(*) part_id INTO var_part_id_in_trip_back FROM digitales_fahrtenbuch_db.trip_back WHERE part_id = NEW.part_id;
    IF var_part_id_in_trip_away != 0
    THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Die part_id wurde bereits einem trip_away zugeordnet!';
	ELSEIF var_part_id_in_trip_back != 0
    THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Die part_id wurde bereits einem trip_back zugeordnet!';
	END IF;
END$$
DELIMITER ;

DROP TRIGGER IF EXISTS `digitales_fahrtenbuch_db`.`location_BEFORE_INSERT`;
DELIMITER $$
USE `digitales_fahrtenbuch_db`$$
CREATE DEFINER = CURRENT_USER TRIGGER `location_BEFORE_INSERT` BEFORE INSERT ON `location` FOR EACH ROW BEGIN
	IF NEW.postalcode REGEXP '^[0-9]{5}$' = 0
    THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Die Postleitzahl muss exakt 5 Zahlen lang sein!';
	END IF;
END$$
DELIMITER ;

DROP TRIGGER IF EXISTS `digitales_fahrtenbuch_db`.`car_BEFORE_INSERT`;
DELIMITER $$
USE `digitales_fahrtenbuch_db`$$
CREATE DEFINER = CURRENT_USER TRIGGER `digitales_fahrtenbuch_db`.`car_BEFORE_INSERT` BEFORE INSERT ON `car` FOR EACH ROW
BEGIN
	IF NEW.vin REGEXP '^[A-Z0-9]{17}$' = 0
    THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Die FIN muss exakt 17 Zeichen lang sein!';
	ELSEIF NEW.VIN REGEXP '[IOQ]+' = 1
    THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'In der FIN dürfen kein I, O oder Q enthalten sein!';
	END IF;
END$$
DELIMITER ;

DROP TRIGGER IF EXISTS `digitales_fahrtenbuch_db`.`user_BEFORE_INSERT`;
DELIMITER $$
USE `digitales_fahrtenbuch_db`$$
CREATE DEFINER = CURRENT_USER TRIGGER `digitales_fahrtenbuch_db`.`user_BEFORE_INSERT` BEFORE INSERT ON `user` FOR EACH ROW
BEGIN
	IF NEW.email REGEXP '^.+@.+\..+$' = 0
    THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'E-Mail-Adresse entspricht keinem gültigen Format!';
	END IF;
END$$
DELIMITER ;

DROP TRIGGER IF EXISTS `digitales_fahrtenbuch_db`.`trip_part_BEFORE_INSERT`;
DELIMITER $$
USE `digitales_fahrtenbuch_db`$$
CREATE DEFINER = CURRENT_USER TRIGGER `digitales_fahrtenbuch_db`.`trip_part_BEFORE_INSERT` BEFORE INSERT ON `trip_part` FOR EACH ROW
BEGIN
	IF NEW.start_location_id = NEW.end_location_id
    THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Der Startpunkt darf nicht identisch mit dem Endpunkt sein, nutzen Sie stattdessen einen Zwischenstopp!';
	END IF;
  IF NEW.start_datetime > NEW.end_datetime
    THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Die Startzeit muss vor der Endzeit liegen!';
  END IF;
END$$
DELIMITER ;

DROP TRIGGER IF EXISTS `digitales_fahrtenbuch_db`.`trip_BEFORE_INSERT`;
DELIMITER $$
USE `digitales_fahrtenbuch_db`$$
CREATE DEFINER = CURRENT_USER TRIGGER `digitales_fahrtenbuch_db`.`trip_BEFORE_INSERT` BEFORE INSERT ON `trip` FOR EACH ROW
BEGIN
	DECLARE var_booking_state INT;
  SELECT state_id INTO var_booking_state FROM digitales_fahrtenbuch_db.booking WHERE booking_id = NEW.booking_id;
    IF var_booking_state != 2
      THEN
		   SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Das Fahrzeug muss erst als abgeholt eingetragen sein, bevor eine Fahrt hinzugefügt werden kann!';
	   END IF;
END$$
DELIMITER ;

DROP TRIGGER IF EXISTS `digitales_fahrtenbuch_db`.`booking_BEFORE_INSERT`;
DELIMITER $$
USE `digitales_fahrtenbuch_db`$$
CREATE DEFINER = CURRENT_USER TRIGGER `digitales_fahrtenbuch_db`.`booking_BEFORE_INSERT` BEFORE INSERT ON `booking` FOR EACH ROW
BEGIN
  IF NEW.start_datetime > NEW.end_datetime
		THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Die Startzeit muss vor der Endzeit liegen!';
	END IF;
END$$
DELIMITER ;

DROP TRIGGER IF EXISTS `digitales_fahrtenbuch_db`.`booking_BEFORE_UPDATE`;
DELIMITER $$
USE `digitales_fahrtenbuch_db`$$
CREATE DEFINER = CURRENT_USER TRIGGER `digitales_fahrtenbuch_db`.`booking_BEFORE_UPDATE` BEFORE UPDATE ON `booking` FOR EACH ROW
BEGIN
	DECLARE var_trips_assigned_to_booking INT;
	IF NEW.state_id = 3
		THEN
      SELECT COUNT(*) trip_id INTO var_trips_assigned_to_booking FROM digitales_fahrtenbuch_db.trip WHERE digitales_fahrtenbuch_db.trip.booking_id = booking_id;
      IF var_trips_assigned_to_booking = 0
		    THEN
          SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Ohne eingetragende Fahrt kann das Fahrzeug nicht zurückgegeben werden!';
		  END IF;
	END IF;
END$$
DELIMITER ;
