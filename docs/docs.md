# Digitales Fahrtenbuch DB - Beschreibung evtl. vorhandener Abfragen/Funktionen/Trigger

## Tabelle: accident
- speichert während der Fahrt aufgetretende Unfälle und Schäden am Fahrzeug
- aktuell nicht genutzt
- Fremdschlüssel 'trip_id' verweist auf Tabelle trip
- keine Trigger

## Tabelle: booking
- speichert Buchungen für Fahrzeuge
- Fremdschlüssel 'car_id' verweist auf Tabelle 'car'
- Fremdschlüssel 'user_id' verweist auf Tabelle 'user'
- Fremdschlüssel 'state_id' verweist auf Tabelle 'booking_state'
- Trigger 'booking_BEFORE_INSERT' verhindert das Einfügen eines Startdatums,
  dass vor dem Enddatum der Buchung liegt
- Trigger 'booking_BEFORE_UPDATE' prüft, ob ein Eintrag zu dieser Buchung im
  Fahrtenbuch erfolgt ist. Ist dies nicht der Fall, darf der Buchungsstatus
  nicht auf 'Zurückgegeben' gesetzt werden.

## Tabelle: booking_state
- speichert Konstanten für den Buchungsstatus

| Buchungsstatus | Bezeichnung   |
|----------------|---------------|
| 1              | Gebucht       |
| 2              | Abgeholt      |
| 3              | Zurückgegeben |

- keine Fremdschlüssel
- keine Trigger

## Tabelle: car
- speichert Fahrzeuge einer Institution
- Fremdschlüssel 'type_id' verweist auf Tabelle 'car_type'
- Fremdschlüssel 'institution_id' verweist auf Tabelle 'institution'
- Trigger 'car_BEFORE_INSERT' verhindert das Einfügen einer ungültigen Fahrzeug-
  identifikationnummer (FIN). Diese muss genau 17 Zeichen aus A-Z und 0-9
  enthalten. Die Buchstaben I, O und Q sind verboten. Für weitere Informationen
  siehe https://de.wikipedia.org/wiki/Fahrzeug-Identifizierungsnummer

## Tabelle: car_type
- speichert Konstanten für Fahrzeugtypen

| Typ_ID | Bezeichnung      |
|--------|------------------|
| 1      | Kleinwagen       |
| 2      | Cabrio           |
| 3      | Coupé            |
| 4      | Geländewagen/SUV |
| 5      | Limousine        |
| 6      | Kombi            |
| 7      | Transporter      |
| 8      | Kleinbus/Van     |
| 9      | Lastkraftwagen   |
| 10     | Mofa             |
| 11     | Leichtkraftrad   |
| 12     | Motorrad         |

- keine Fremdschlüssel
- keine Trigger

## Tabelle: gas
- speichert Tankgänge während einer Fahrt
- aktuell nicht genutzt
- Fremdschlüssel 'trip_id' verweist auf Tabelle 'trip'
- keine Trigger

## Tabelle: has_passenger
- speichert Zuordnung zwischen Fahrten und Passagieren
- aktuell nicht genutzt
- Fremdschlüssel 'trip_id' verweist auf Tabelle 'trip'
- Fremdschlüssel 'passenger_id' verweist auf Tabelle 'passenger_id'
- keine Trigger

## Tabelle: has_role
- speichert Zuordnung zwischen Nutzern und Rollen
- Fremdschlüssel 'user_id' verweist auf Tabelle 'user'
- Fremdschlüssel 'role_id' verweist auf Tabelle 'role'
- keine Trigger

## Tabelle: institution
- speichert Institutionen wie Unternehmen oder Behörden
- Fremdschlüssel 'location' verweist auf Tabelle 'location'
- keine Trigger

## Tabelle: location
- speichert Orte
- keine Fremdschlüssel
- Trigger 'location_BEFORE_INSERT' verhindert das Einfügen von Postleitzahlen
  die nicht genau aus 5 Zahlen bestehen

## Tabelle: passenger
- speichert Beifahrer
- aktuell nicht genutzt
- keine Fremdschlüssel
- keine Trigger

## Tabelle: role
- speichert Konstanten für Rollen (Berechtigungen) von Nutzern

| Rollen_ID | Bezeichnung |
|-----------|-------------|
| 1         | Fahrer      |
| 2         | Controller  |
| 3         | Buchhalter  |
| 4         | Dritter     |

- keine Fremdschlüssel
- keine Trigger

## Tabelle: sessions
- speichert eine Nutzersession serverseitig
- Fremdschlüssel 'user_id' verweist auf Tabelle 'user'
- Fremdschlüssel 'trip_id' verweist auf Tabelle 'trip'
- keine Trigger

## Tabelle: trip
- speichert eine Fahrt
- Fremdschlüssel 'trip_away_id' verweist auf Tabelle 'trip_away'
- Fremdschlüssel 'trip_back_id' verweist auf Tabelle 'trip_back'
- Fremdschlüssel 'car_id' verweist auf Tabelle 'car'
- Fremdschlüssel 'booking_id' verweist auf Tabelle 'booking'
- Trigger 'trip_BEFORE_INSERT' verhindert das Einfügen eines Fahrzeuges, dass
  noch nicht abgeholt wurde (Buchungsstatus nicht gleich 'Abgeholt')

## Tabelle: trip_away
- speichert die Zuordnung eines Fahrtabschnittes als Hinfahrt
- Fremdschlüssel 'part_id' verweist auf Tabelle 'trip_part'
- Trigger 'trip_away_BEFORE_INSERT' verhindert das doppelte Einfügen eines
  Fahrtabschnittes, der bereits als Rückfahrt oder Zwischenstopp gespeichert ist

## Tabelle: trip_back
- speichert die Zuordnung eines Fahrtabschnittes als Rückfahrt
- Fremdschlüssel 'part_id' verweist auf Tabelle 'trip_part'
- Trigger 'trip_back_BEFORE_INSERT' verhindert das doppelte Einfügen eines
  Fahrtabschnittes, der bereits als Hinfahrt oder Zwischenstopp gespeichert ist

## Tabelle: trip_between
- speichert die Zuordnung eines Fahrtabschnittes als Zwischenstopp
- Fremdschlüssel 'part_id' verweist auf Tabelle 'trip_part'
- Fremdschlüssel 'trip_id' verweist auf Tabelle 'trip'
- Trigger 'trip_between_BEFORE_INSERT' verhindert das doppelte Einfügen eines
  Fahrtabschnittes, der bereits als Hinfahrt oder Rückfahrt gespeichert ist

## Tabelle: trip_part
- speichert einen Fahrtabschnitt, der noch nicht zugeordnet wurde
- Fremdschlüssel 'start_location_id' verweist auf Tabelle 'location'
- Fremdschlüssel 'end_location_id' verweist auf Tabelle 'location'
- Trigger 'trip_part_BEFORE_INSERT' verhindert das Einfügen von Fahrt-
  abschnitten, bei denen der Start- und Endort gleich ist oder bei denen
  die Endzeit vor der Startzeit liegt

## Tabelle: user
- speichert Nutzer
- Fremdschlüssel 'institution' verweist auf Tabelle 'institution'
- Trigger 'user_BEFORE_INSERT' verhindert das Einfügen von Nutzern mit
  ungültiger E-Mail Adresse
