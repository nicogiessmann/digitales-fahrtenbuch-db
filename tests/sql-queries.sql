-- Tests if database works like expected

USE `digitales_fahrtenbuch_db`;

-- insert location
INSERT INTO `location` (`street`, `house_number`, `town`, `postalcode`) VALUES ("Franklin Street", "2757", "Dothan", "36303");
INSERT INTO `location` (`street`, `house_number`, `town`, `postalcode`) VALUES ("Luke Lane", "1961", "Logansport", "46947");
INSERT INTO `location` (`street`, `house_number`, `town`, `postalcode`) VALUES ("Elk Avenue", "1025", "Southfield", "48075");

-- insert institution
INSERT INTO `institution` (`title`, `location`) VALUES ("Minsterium für Testzwecke", "1");

-- insert user
INSERT INTO `user` (`first_name`, `last_name`, `email`, `password`, `institution`) VALUES ("Erika", "Mustermann", "erika.mustermann@mustermail.de", "passwort", "1");

-- insert car
INSERT INTO `car` (`model`, `manufacturer`, `vin`, `seats`, `milage`, `first_registration`, `type_id`) VALUES ("Kangoo", "Renault", "W0L000051T2123456", "5", "193250", "2007-10-01", "8");

-- insert booking
INSERT INTO `booking` (`start_datetime`, `end_datetime`, `car_id`, `user_id`) VALUES ("2020-03-20 11:00:00", "2020-03-20 16:00:00", "1", "1");

-- update booking
UPDATE `booking` SET `state_id` = 2 WHERE `booking_id` = "1";

-- insert trips
INSERT INTO `trip_part` (`start_datetime`, `end_datetime`, `distance`, `start_location_id`, `end_location_id`) VALUES ("2020-03-20 11:15:00", "2020-03-20 12:21:00", "45.1", "1", "2");
INSERT INTO `trip_part` (`start_datetime`, `end_datetime`, `distance`, `start_location_id`, `end_location_id`) VALUES ("2020-03-20 13:02:00", "2020-03-20 13:27:00", "20.3", "2", "3");
INSERT INTO `trip_part` (`start_datetime`, `end_datetime`, `distance`, `start_location_id`, `end_location_id`) VALUES ("2020-03-20 13:45:00", "2020-03-20 14:01:00", "20.5", "3", "2");
INSERT INTO `trip_part` (`start_datetime`, `end_datetime`, `distance`, `start_location_id`, `end_location_id`) VALUES ("2020-03-20 15:12:00", "2020-03-20 15:56:00", "43.6", "2", "1");

-- insert trip_away (first define a trip_part then assign to trip_away)
INSERT INTO `trip_away` (`part_id`) VALUES ("1");

-- insert trip_back
INSERT INTO `trip_back` (`part_id`) VALUES ("4");

-- insert trip (second to the last step after adding of trip_away and trip_back)
INSERT INTO `trip` (`name`, `trip_away_id`, `trip_back_id`, `user_id`, `car_id`, `booking_id`) VALUES ("Besprechung", "1", "4", "1", "1", "1");

-- insert trip_between (after trip is added, confusing I know but thats database consistency)
INSERT INTO `trip_between` (`part_id`, `trip_id`) VALUES ("2", "1");
INSERT INTO `trip_between` (`part_id`, `trip_id`) VALUES ("3", "1");

-- insert passenger
INSERT INTO `passenger` (`first_name`, `last_name`) VALUES ("Peer", "Anhalter");

-- insert has_passenger
INSERT INTO `has_passenger` (`trip_id`, `passenger_id`) VALUES ("1", "1");

-- insert gas
INSERT INTO `gas` (`liter`, `amount`, `trip_id`) VALUES ("56.1", "98.50", "1");

-- insert accident
INSERT INTO `accident` (`description`, `trip_id`) VALUES ("Schramme beim Einparken verursacht.", "1");

-- update booking
UPDATE `booking` SET `state_id` = 3 WHERE `booking_id` = "1";
