Maintainer: Nico Gießmann  
Kontakt: <nigi4901@th-wildau.de>  
Zuletzt bearbeitet: 16.04.20  


# Datenbank für das Digitale Fahrtenbuch und Fuhrparkmanagement

Mithilfe dieses Repositories lässt sich mit Docker die Datenbank für das
Digitales Fuhrparkmanagement und Fahrtenbuch erstellen (digitales_fahrtenbuch_db).
Enthalten ist auch die Datenbank zur Archivierung (archive_db).

Der Docker-Container ist auf jedem Server mit 64-Bit Betriebssystem lauffähig.
Für Testzwecke wurde die unten stehende Installationsanleitung auf einem Raspberry Pi Gen. 4
mit Gentoo-Linux und einem MacBook Pro mit macOS Catalina 10.15.3 getestet.
Darüber hinaus ist die Datenbank unter folgenden Attributen öffentlich abrufbar:

**Hostname:** ubuntu.w0pthk1dfad6rcai.myfritz.net  
**Port:** 3306  
**Username:** viewer *(ausschließlich select-Anweisungen sind ausführbar, für weitere
  Berechtigungen wenden sie sich bitte an den Maintainer dieses GitLab-Repositories
  unter den oben genannten Kontaktdaten)*  
**Passwort:** 7;7N8*GF/Gmc6gY$

**Aktueller Status:** offline  

## Installationsanleitung

- Klonen des Repositories

    ```
    $ git clone git@gitlab.com:nicogiessmann/digitales-fahrtenbuch-db.git
    ```

- Erstelle einen leeren Ordner für die Daten des Datensatzes

    ```
    $ mkdir data
    $ mkdir data/db
    ```

- Setzen des **MYSQL_ROOT_PASSWORD** in der *.env.public* Datei, sowie der **Nutzerpasswörter** in *db.conf.sql.public*.
Anschließend können sie die Endung *.public* entfernen.

- Unter Linux-Systemen kann mithilfe der UFW leicht ein wirkungsvoller Schutz der Datenbank gewährleistet werden.
Dafür führen sie das Skript *firewall.conf.public* aus. Vorher können sie in dem Skript die IP-Adresse des
Administrationsrechners setzen. Außerdem muss die UFW über den auf ihrem System verfügbaren Paketmanager installiert sein.
Anschließend können sie die Endung *.public* entfernen.

  ```
  $ bash firewall.conf
  ```

- Herunterladen und installieren von Docker und Docker-Compose z. B. über <https://www.docker.com>

- Datenbank-Container starten und Datenbanken erstellen

    ```
    $ sudo docker-compose up -d
    $ sudo docker exec -it mysql-server bash
    $ mysql -h localhost -u root -p
    # Geben sie das von ihren in der .env-Datei gesetze Passwort ein
    $ mysql> source /db.conf.sql;
    $ mysql> source /digitales_fahrtenbuch_db.sql;
    $ mysql> source /archive_db.sql;
    $ mysql> source /constants.sql;
    $ mysql> exit;
    $ exit
    ```

- Der Datenbankserver läuft nun und hört auf Port 3306 (anpassbar in der **docker-compose.yml**). Der Zugriff aus dem Internet setzt eine aktive Netzwerkverbindung, sowie eventuelle Portfreigaben bei ihrem Router voraus.

Bei Fragen wenden sie sich gerne an den Maintainer.
