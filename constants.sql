-- Insert constants with fixed IDs and values

USE digitales_fahrtenbuch_db;

INSERT INTO `car_type` (`type_id`, `name`) VALUES ('1', 'Kleinwagen');
INSERT INTO `car_type` (`type_id`, `name`) VALUES ('2', 'Cabrio');
INSERT INTO `car_type` (`type_id`, `name`) VALUES ('3', N'Coupé');
INSERT INTO `car_type` (`type_id`, `name`) VALUES ('4', N'Geländewagen/SUV');
INSERT INTO `car_type` (`type_id`, `name`) VALUES ('5', 'Limousine');
INSERT INTO `car_type` (`type_id`, `name`) VALUES ('6', 'Kombi');
INSERT INTO `car_type` (`type_id`, `name`) VALUES ('7', 'Transporter');
INSERT INTO `car_type` (`type_id`, `name`) VALUES ('8', 'Kleinbus/Van');
INSERT INTO `car_type` (`type_id`, `name`) VALUES ('9', 'Lastkraftwagen');
INSERT INTO `car_type` (`type_id`, `name`) VALUES ('10', 'Mofa');
INSERT INTO `car_type` (`type_id`, `name`) VALUES ('11', 'Leichtkraftrad');
INSERT INTO `car_type` (`type_id`, `name`) VALUES ('12', 'Motorrad');

INSERT INTO `role` (`role_id`, `name`) VALUES ('1', 'Fahrer');
INSERT INTO `role` (`role_id`, `name`) VALUES ('2', 'Controller');
INSERT INTO `role` (`role_id`, `name`) VALUES ('3', 'Buchhalter');
INSERT INTO `role` (`role_id`, `name`) VALUES ('4', 'Dritter');

INSERT INTO `booking_state` (`state_id`, `name`) VALUES ('1', 'Gebucht');
INSERT INTO `booking_state` (`state_id`, `name`) VALUES ('2', 'Abgeholt');
INSERT INTO `booking_state` (`state_id`, `name`) VALUES ('3', N'Zurückgegeben');

USE archive_db;

INSERT INTO `car_type` (`type_id`, `name`) VALUES ('1', 'Kleinwagen');
INSERT INTO `car_type` (`type_id`, `name`) VALUES ('2', 'Cabrio');
INSERT INTO `car_type` (`type_id`, `name`) VALUES ('3', N'Coupé');
INSERT INTO `car_type` (`type_id`, `name`) VALUES ('4', N'Geländewagen/SUV');
INSERT INTO `car_type` (`type_id`, `name`) VALUES ('5', 'Limousine');
INSERT INTO `car_type` (`type_id`, `name`) VALUES ('6', 'Kombi');
INSERT INTO `car_type` (`type_id`, `name`) VALUES ('7', 'Transporter');
INSERT INTO `car_type` (`type_id`, `name`) VALUES ('8', 'Kleinbus/Van');
INSERT INTO `car_type` (`type_id`, `name`) VALUES ('9', 'Lastkraftwagen');
INSERT INTO `car_type` (`type_id`, `name`) VALUES ('10', 'Mofa');
INSERT INTO `car_type` (`type_id`, `name`) VALUES ('11', 'Leichtkraftrad');
INSERT INTO `car_type` (`type_id`, `name`) VALUES ('12', 'Motorrad');

INSERT INTO `role` (`role_id`, `name`) VALUES ('1', 'Fahrer');
INSERT INTO `role` (`role_id`, `name`) VALUES ('2', 'Controller');
INSERT INTO `role` (`role_id`, `name`) VALUES ('3', 'Buchhalter');
INSERT INTO `role` (`role_id`, `name`) VALUES ('4', 'Dritter');

INSERT INTO `booking_state` (`state_id`, `name`) VALUES ('1', 'Gebucht');
INSERT INTO `booking_state` (`state_id`, `name`) VALUES ('2', 'Abgeholt');
INSERT INTO `booking_state` (`state_id`, `name`) VALUES ('3', N'Zurückgegeben');
